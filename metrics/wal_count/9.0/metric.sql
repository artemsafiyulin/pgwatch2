SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  count(*) from pg_ls_dir('pg_xlog') AS count;

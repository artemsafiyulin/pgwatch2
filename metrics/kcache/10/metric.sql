SELECT /* pgwatch2_generated */
  (extract(epoch from now()) * 1e9)::int8 as epoch_ns,
  queryid::text as tag_queryid,
  exec_user_time as user_time,
  exec_system_time as system_time,
  exec_minflts as minflts,
  exec_majflts as majflts,
  exec_reads as reads,
  exec_writes as writes,
  exec_nvcsws as nvcsws,
  exec_nivcsws as nivcsws
FROM pg_stat_kcache()
WHERE dbid = (select oid from pg_database where datname = current_database())
ORDER BY queryid;
